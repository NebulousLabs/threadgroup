module gitlab.com/NebulousLabs/threadgroup

go 1.13

require (
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
)
